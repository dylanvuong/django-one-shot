from django.shortcuts import render
from .models import TodoList
from django.urls import path
from .views import todo_list_list, todo_list_detail, todo_list_add


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    context = {'todo_lists': todo_lists}
    return render(request, 'todos/todo_list_list.html', context)

app_name = 'todos'

urlpatterns = [
    path('', todo_list_list, name='todo_list_list'),
    path('<int:id>/', todo_list_detail, name='todo_list_detail'),
    path('add/', todo_list_add, name='todo_list_add'),
]
