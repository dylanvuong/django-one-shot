from django.shortcuts import render, redirect, get_object_or_404
from .models import TodoList
from .forms import TodoListForm
from datetime import date


def todo_list_list(request):
    todo_lists = TodoList.objects.all()
    return render(request, 'todos/todo_list_list.html', {'todo_lists': todo_lists})


def todo_list_detail(request, id):
    todo_list = get_object_or_404(TodoList, id=id)
    context = {'todo_list': todo_list, 'current_date': date.today()}
    return render(request, 'todos/todo_list_detail.html', context)


def create_todo_list(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    return render(request, 'todos/create_todo_list.html', {'form': form})


def todo_list_add(request):
    if request.method == 'POST':
        form = TodoListForm(request.POST)
        if form.is_valid():
            todo_list = form.save()
            return redirect("todo_list_detail", id=todo_list.id)
    else:
        form = TodoListForm()
    context = {'form': form}
    return render(request, 'todos/todo_list_add.html', context)