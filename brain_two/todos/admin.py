from django.contrib import admin
from .models import TodoList, TodoItem
# Register your models here.


class TodoListAdmin(admin.ModelAdmin):
    list_display =(
        "id",
        "name",        
    )


class TodoItemAdmin(admin.ModelAdmin):
    list_display = (
        "task", 
        "due_date", 
        "completed",
    )


admin.site.register(TodoList)


admin.site.register(TodoItem)   